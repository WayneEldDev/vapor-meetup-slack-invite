import Vapor
import SwiftyRequest

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    // Basic "It works" example
//    router.get { req in
//        return "It works!"
//    }
    

    
    router.get { req -> Future<View> in
        return try req.view().render("index")
    }
  
    
    router.get("thanks") { req -> Future<View> in
        return try req.view().render("thanks")
    }
    
    router.get("error") { req -> Future<View> in
        return try req.view().render("error")
    }
    

    
    struct Email: Content {
        var address: String
    }
    
    struct SlackResponse: Codable {
        var ok: Bool
    }
    
    router.post("slack") { req -> Future<Response> in

        return try req.content.decode(Email.self).map(to: Response.self) { email in
            
            let emailAddress = email.address
            
            if emailAddress.isValidEmail() {

                let res = try req.client().post("https://slack.com/api/users.admin.invite?token=xoxp-633061623618-646655171462-638139000545-ffe11ff261020df5bb627b14df9c1ba7&email=\(emailAddress)")
                
                return req.redirect(to: "/thanks")
                
            } else {

                print("error")
             return req.redirect(to: "/error")
 
            }
            
        }
    }
}


extension String {
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
}
